#!/bin/bash
f=$1
encoding=$(file -i $f | cut -d'=' -f2)
if [[ $encoding = "binary" || $encoding = "utf-8" || $encoding = "us-ascii" ]]; then
 echo "Skipping $f ... $encoding"
 exit 0
elif [[ $encoding = "iso-8859-1" ||  $encoding = "iso-8859-15" ||  $encoding = "unknown-8bit" ]]; then 
 echo "Converting $f ... from $encoding to utf8"
 iconv -f $encoding -t utf-8 $f > "$f".utf8; ERROR=$?
 mv $f.utf8 $f 
else
 echo "Checked $f ... $encoding" 
fi

if [[ $ERROR != 0 ]]; then
 echo "Error on $f"
fi

ERROR=0