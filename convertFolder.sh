#!/bin/bash
folder="C:/development/eclipse_workspaces/workspace_ncm/ncm";
toUTF="C:/development/scripts/bash/to-utf8.sh";
find $folder -not -path "*git/*" -not -path "*svn/*" -not -path "*settings/*" -not -path "*metadata/*" -not -path "*bin/*" -not -path "*build/*" -not -path "*web_minified/*" -not -path "*images/*" -not -name "*.class" -not -name "*.jar" -not -name "*.war" -not -name "*.zip" -not -name "*.png" -not -name "*.jpg" -not -name "*.gif" | xargs -n 1 $toUTF